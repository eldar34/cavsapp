from django.apps import AppConfig


class MoviesConfig(AppConfig):
    name = 'cavs'
    verbose_name = "Фильмы"
